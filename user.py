from time import time
import logging

import grcconf as g
import emotes as e
import wallet as w
import queries as q
import docs

class User:
    def __init__(self, uid, **k):
        self.usrID = uid
        self.balance = k.get('balance', 0.0)
        self.unconfirmed = k.get('unconfirmed', 0.0)
        self.active_tx = k.get('lastTX', [None, 0, None]) # [amount, timestamp, txid]
        self.donations = k.get('donations', 0.0)
        self.last_faucet = k.get('last_faucet', 0)
        self.address = k.get('address', None)
        self.dm_enable = k.get('dm_enable', None)
        self.stakes = k.get('stakes', None)


    async def withdraw(self, amount, addr, fee, donation=False, comment=None):
        validation_result = self.can_transact(amount, fee, net_tx=True, generous=donation)
        if isinstance(validation_result, bool):
            txid = await w.tx(addr, amount-fee, comment)
            if isinstance(txid, str):
                tx_time = round(time())
                self.active_tx = [amount, tx_time, txid.replace('\n', '')]
                self.balance -= amount
                if donation:
                    self.donations += amount - fee
                await q.save_user(self)
                if fee == g.tx_fee:
                    await q.add_to_fee_pool(fee)
                logging.info('Transaction successfully made with txid: %s', txid)
                return docs.net_tx_success.format(e.GOOD, round(amount, 8), fee, txid, '\n\nYour new balance is {} GRC.'.format(round(abs(self.balance), 2)))
            logging.error('Failed transaction. Addr: %s, Amt: %s, exit_code: %s', addr, amount, txid)
            return docs.tx_error
        return validation_result


    async def send_internal_tx(self, other_user, amount, donation=False):
        validation_result = self.can_transact(amount, 0)
        await q.log_transaction(self, other_user, amount)
        if isinstance(validation_result, bool):
            self.balance -= amount
            other_user.balance += amount
            if donation:
                self.donations += amount
            await q.save_user([self, other_user])
            return docs.internal_tx_success.format(e.GOOD, amount)
        return validation_result


    def next_net_tx(self):
        return self.active_tx[1]+g.BLK_TIME*g.tx_timeout


    def next_donate(self):
        return self.active_tx[1]+g.BLK_TIME*g.donate_timeout


    def next_fct(self):
        return self.last_faucet+3600*g.FCT_REQ_LIM


    def can_net_tx(self):
        return time() > self.next_net_tx()


    def can_donate(self):
        return time() > self.next_donate()


    def can_faucet(self):
        return time() > self.next_fct()


    def can_transact(self, amount, fee, net_tx=False, generous=False):
        if net_tx:
            if generous:
                if not self.can_donate(): return docs.wait_confirm;
            else:
                if not self.can_net_tx(): return docs.wait_confirm;
        if amount is None: return docs.invalid_val;
        if amount > round(self.balance, 8): return docs.insufficient_funds;
        if fee != 0 and amount <= (fee + g.MIN_TX): return docs.more_than_fee_and_min;
        return True # If values passed all checks
