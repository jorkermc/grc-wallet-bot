CREATE SCHEMA IF NOT EXISTS grcbot;

USE grcbot;


CREATE TABLE IF NOT EXISTS udb (
  uid VARCHAR(18),
  address VARCHAR(34),
  last_faucet BIGINT,
  balance DOUBLE,
  donations DOUBLE,
  lastTX_amt DOUBLE,
  lastTX_time BIGINT,
  lastTX_txid VARCHAR(70),
  dm_enable bit(1),
  stakes DOUBLE,

  PRIMARY KEY (uid)
);


CREATE TABLE IF NOT EXISTS admin_claims (
  txid VARCHAR (70),
  amount DOUBLE,

  PRIMARY KEY (txid)
);


CREATE TABLE IF NOT EXISTS blacklist (
  uid VARCHAR(18),
  channel BIT(1),

  PRIMARY KEY (uid)
);


CREATE TABLE IF NOT EXISTS channels (
  channel VARCHAR(18),
  faucetBanRule bit(1) DEFAULT 1,

  PRIMARY KEY (channel)
);


CREATE TABLE IF NOT EXISTS adverts (
  msgID INT UNSIGNED NOT NULL AUTO_INCREMENT,
  message TINYTEXT,
  ref_email VARCHAR(254),
  active BOOLEAN,

  PRIMARY KEY (msgID)
);


CREATE TABLE IF NOT EXISTS donors (
  name VARCHAR(50),
  address VARCHAR(34),

  PRIMARY KEY (name)
);


CREATE TABLE IF NOT EXISTS deposits (
  txid VARCHAR(70),
  amount DOUBLE,
  uid VARCHAR(18),
  confirmed BOOLEAN DEFAULT 0,

  PRIMARY KEY (txid, uid)
);


CREATE TABLE IF NOT EXISTS stakes (
  txid VARCHAR(70),
  amount DOUBLE,
  time BIGINT,

  PRIMARY KEY (txid)
);


CREATE TABLE IF NOT EXISTS tx_log (
  sender VARCHAR(18),
  receiver VARCHAR(18),
  amount DOUBLE,
  time BIGINT
);


-- statements for changes to database structure from previous versions
ALTER TABLE adverts DROP COLUMN IF EXISTS ref_discordID;
ALTER TABLE adverts DROP COLUMN IF EXISTS ref_discord_name;
ALTER TABLE deposits ADD COLUMN IF NOT EXISTS confirmed BOOLEAN DEFAULT 1;
ALTER TABLE channels ADD COLUMN IF NOT EXISTS faucetBanRule bit(1) DEFAULT 1;
-- UPDATE deposits SET confirmed=1;


INSERT INTO udb VALUES ('FAUCET', '', 0, 0, 0, 0, 0, '', 0, 0);
INSERT INTO udb VALUES ('RAIN', '', 0, 0, 0, 0, 0, '', 0, 0);

INSERT INTO admin_claims VALUES ('PENDING', 0);
