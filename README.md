# GRC-Wallet-Bot

Discord bot by [Delta](https://github.com/delta1512) for the [Gridcoin Discord chat](https://discord.me/page/gridcoin).

This bot aims to be a third part wallet service for the Gridcoin cryptocurrency and allows people to deposit, store, withdraw, donate and get Gridcoins. The bot contains its own faucet to encourage new users and provides an easy and interactive interface for generosity and donating.

## Requirements

* Python 3.7

* A remote MYSQL database (such as [MariaDB](https://mariadb.com/))

* An active Gridcoin Client

* Filled out configuration file

* A record in the DB with the userID `'FAUCET'` and a preset GRC address

* A record in the DB with the userID `'RAIN'` and a preset GRC address

* [scavenger.py](./scavenger.py) running as a background process

## Database Setup

Run the included [SQL](./grcbot.sql) script.

### Feature ideas:

These are just ideas, not all of them may be implemented in the future. Any suggestions are welcome.

- [ ] Interactive donation presentation

- [ ] Site or command to view donation ranks

- [x] Faucet

- [x] Random donations

- [ ] Dice and other assorted features

- [X] Rain/soak

- [X] Help topics such as getting started with GRC

- [ ] Block exploring features (under construction)

- [ ] BOINC stats

- [X] Private messaging support

- [X] FAQ interface

- [ ] Static withdraw address option

- [X] %time function to show eligibility of %faucet, %withdraw and %donate

- [ ] Random facts

- [X] %give and %rain in all chats

### FAQ Ideas:

- [ ] Linux compilation guide

- [X] Third-party wallets

- [X] Exchanges

- [X] Faucets

- [ ] History of Gridcoin
