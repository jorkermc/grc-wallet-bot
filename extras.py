from math import ceil, isfinite
import discord
import aiohttp
import random as r
import time
import io
import re
from datetime import timezone
from hashlib import sha512
from os import path

import qrcode

import queries as q
import grcconf as g
import emotes as e
import wallet as w
import help_docs
import docs
import FAQ


user_locks = {}
game_names = {}
game_pixels = {}
last_req = 0
advert_cycle = 0


def time_display(t):
    if t > 86400: #1 day
        return time.strftime("%d Days %H Hours %M Minutes %S Seconds", time.gmtime(t))
    return time.strftime("%H Hours %M Minutes %S Seconds", time.gmtime(t))


def amt_filter(inp):
    try:
        inp = float(inp)
        if (inp < g.MIN_TX) or not isfinite(inp):
            return None
        else:
            return round(inp, 8)
    except:
        return None


def stringify_dict_data(data):
    acc = ''
    for key in data:
        acc += key + ': ' + str(data[key]) + '\n'
    return f'```{acc}```'


def get_discord_name(client, uid):
    uid = int(uid)
    for member in client.get_all_members():
        if uid == member.id:
            return member.name
    return str(uid)


async def dump_cfg():
    fct_info = await q.get_bal('FAUCET')
    rain_info = await q.get_bal('RAIN')
    last_stake = await q.get_last_stake()

    data_config = f'''Withdraw fee: `{g.tx_fee} GRC`
Min. transfer limit: `{g.MIN_TX} GRC`
Withdrawal confs: `{g.tx_timeout}`
Faucet timeout: `{g.FCT_REQ_LIM} Hours`
Users: `{len(await q.get_addr_uid_dict())}`
24h transaction vol: `{round(await q.get_24h_tx_volume(), 1)} GRC`
Total donations: `{round(await q.get_total_donations(), 1)} GRC`
'''

    data_wallet =f'''GRC client version: `{await w.get_version()}`
Withdrawals: {e.ONLINE if await w.is_unlocked() else e.OFFLINE}
Staking: {e.ONLINE if await w.is_unlocked() else e.OFFLINE}
Deposits: {e.ONLINE if w.deposits_online() else e.OFFLINE}
'''

    data_staking = f'''Last stake: `{time_display(time.time() - last_stake["time"])} Ago`
Total stakes: `{await q.get_num_stakes()}`'''

    data_rain_and_faucet = f'''Faucet funds: `{round(fct_info[0], 8)} GRC ({fct_info[1]})`
Rain funds: `{round(rain_info[0], 8)} GRC ({rain_info[1]})`'''

    embed = discord.Embed(title='Bot is online', colour=discord.Colour.green())
    embed.add_field(name='Configuration:', value=data_config)
    embed.add_field(name='Wallet Status:', value=data_wallet)
    embed.add_field(name='Staking Info:', value=data_staking)
    embed.add_field(name='Rain and Faucet Status:', value=data_rain_and_faucet)
    return embed


async def donate(user_obj, selection, amount):
    selection -= 1
    donation_accts = await q.get_donors()
    if 0 <= selection < len(donation_accts):
        acct_dict = donation_accts[selection]
        selection_name = list(acct_dict.keys())[0]
        addr = acct_dict[selection_name]
        result = await user_obj.withdraw(amount, addr, g.net_fee, True)
        if result.startswith(e.GOOD):
            return result + docs.donation_recipient.format(selection_name)
        return result
    return docs.invalid_selection


async def rdonate(user_obj, amount):
    return await donate(user_obj, r.randint(1, len(await q.get_donors())), amount)


def index_displayer(header, index):
    acc = ''
    for count, acct in enumerate(index):
        name = list(acct.keys())[0]
        acc += '\n{}. {}'.format(str(count+1), name)
    return '{}```{}```'.format(header, acc[1:])


async def faucet(uid):
    global user_locks, advert_cycle
    if uid in user_locks:
        return f'{e.CANNOT}Please wait'
    user_locks[uid] = True
    ctime = round(time.time())
    amount = round(r.uniform(g.FCT_MIN, g.FCT_MAX), 8)
    user_obj, exit_code = await q.faucet_operations(uid, amount, ctime)
    user_locks.pop(uid)
    if exit_code == 1:
        return '{}Request too recent. Faucet timeout is {} hours. Try again in: {}'.format(e.CANNOT, g.FCT_REQ_LIM, time_display(ceil(user_obj.next_fct()-ctime)))
    if exit_code == 2:
        return f'{e.DOWN}Unfortunately the faucet balance is too low. Try again soon.'
    await q.log_transaction('FAUCET', uid, amount)
    advert_cycle += 1
    return docs.claim.format(e.GOOD, amount, round(user_obj.balance, 8), await q.get_faucet_advert(advert_cycle))


def get_qr(string):
    qr = qrcode.QRCode(
         version=1,
         error_correction=qrcode.constants.ERROR_CORRECT_L,
         box_size=10,
         border=2)
    qr.add_data(string)
    qr.make(fit=True)
    savedir = io.BytesIO()
    img = qr.make_image(fill_color='#5c00b3', back_color='white')
    img.save(savedir, format="PNG")
    savedir.seek(0)
    return savedir


def help_interface(query):
    try:
        return help_docs.help_dict[query]
    except KeyError:
        return help_docs.help_main()


def faq(query):
    query -= 1
    if 0 <= query < len(FAQ.index):
        article = FAQ.index[query]
        return article[list(article.keys())[0]]
    return docs.invalid_selection


def faq_index():
    return discord.Embed(
        title=docs.faq_msg,
        description=index_displayer('', FAQ.index).replace('`', '')
                    + '\n*Thanks to LavRadis and Foxifi for making these resources.*',
        colour=discord.Colour.purple())

async def show_top_10_donators(client):
    top_10 = await q.get_top_10_donators()
    final = ''
    for i, d in enumerate(top_10):
        name = get_discord_name(client, d[0])
        final += f'{i+1}. {name} (`{d[1]} GRC`)\n'
    return discord.Embed(
        title='Top 10 donators',
        description=f'{final}',
        colour=discord.Colour.purple())


async def show_block(height):
    data = await w.get_block(height)
    if data is None:
        return docs.wallet_data_error
    return stringify_dict_data(data)


async def show_superblock():
    data = await w.get_last_superblock()
    if data is None:
        return docs.wallet_data_error
    return stringify_dict_data(data)


async def stake_calculator(amount):
    supply = (await w.query('getinfo', []))['moneysupply']
    amount = amt_filter(amount)
    if amount:
        if g.MIN_STK <= amount <= supply:
            diff = (await w.query('getblockstats', [1, 960]))['averages']['posdiff']
            etts = (16000/amount) * diff
            #75% bias added to not over-estimate
            annual = g.STK_REWARD * (365/etts) * (0 if etts > 365 else 0.75)
            return docs.stake_info_template.format(e.MONEY, amount, round(etts, 2), round(annual, 3))
    return docs.invalid_val


async def get_viewer_statistics(client):
    n_total_users = 0
    n_visible_users = 0
    n_bot_users = len(await q.get_addr_uid_dict())
    n_servers = len(client.guilds)
    wb_channels = list((await q.get_main_chans()).keys())

    # Get the total amount of users
    for g in client.guilds:
        n_total_users += len(g.members)
        # Get the number of users in each wallet bot channel
        for c in g.text_channels:
            if str(c.id) in wb_channels:
                n_visible_users += len(c.members)

    return discord.Embed(title='Discord user statistics', colour=discord.Colour.orange(),
            description=f'''
- Discord users the bot can see: `{n_total_users}`
- Discord users in wallet bot channels: `{n_visible_users}`
- Users with an account on the bot: `{n_bot_users}`
- Servers the bot is in: `{n_servers}`
''')




async def fetch_grids():
    global game_names
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(g.gp_url + 'getgrids') as resp:
                assert resp.status == 200
                raw_games_data = (await resp.json())['games']
        games = ''
        for game in raw_games_data:
            game_names[game['name'].lower().replace(' ', '-')] = game['address']
            games += docs.gp_game_row_template.format(
                game['name'].replace(' ', '-'),
                f'{g.gp_url}play?addr={game["address"]}',
                game['width'],
                game['height'],
                game['description'],
                game['address']
            )
        return discord.Embed(title='Active games', colour=discord.Colour.purple(), description=games)
    except:
        return docs.failed_fetch


async def fetch_grid(name):
    if name in game_names:
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(g.gp_url + 'imgrefresh') as resp:
                    assert resp.status == 200
        except:
            # A cached image is ok
            pass
        eb = discord.Embed(title='Click "open original" for latest image', colour=discord.Colour.purple())
        eb.set_image(url=f'{g.gp_url}static/img/{game_names[name]}.png')
        return eb
    return discord.Embed(description=docs.invalid_val)


async def get_game_pixels(game_address):
    global last_req
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(g.gp_url + f'getpixels?addr={game_address}') as resp:
                assert resp.status == 200
                last_req = time.time()
                return (await resp.json())['grid']
    except:
        return 'Failed to get pixels'


async def gp_place(name, x, y, col, user_obj):
    global game_pixels, last_req
    if not name in game_names:
        return docs.invalid_val
    if not re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', col):
        return docs.invalid_val
    if not ((last_req+g.BLK_TIME > time.time()) and (name in game_pixels)):
        # Only fetch if not cached and timed-out
        results = await get_game_pixels(game_names[name])
        if type(results) is str:
            # Return error
            return f'{e.ERROR} Something went wrong trying to get the grid information.'
        game_pixels[name] = results
    cost = game_pixels[name][x][y]['price']
    # 5x net fee because I anticipate many dust UTXOs will be used
    return await user_obj.withdraw(
                    cost+(g.net_fee*5),
                    game_names[name],
                    g.net_fee*5,
                    donation=True,
                    comment=f'{x} {y} {col}'
                    )


def moon():
    clock = ':clock{}:'.format(r.randint(1, 12))
    day = '{}{}'.format(r.choice(e.NUMS[:3]), r.choice(e.NUMS[1:]))
    month0 = r.choice(e.NUMS[:2])
    month1 = r.choice(e.NUMS[:3]) if e.NUMS.index(month0) == 1 else r.choice(e.NUMS)
    month = month0 + month1
    year = ':two::zero:{}{}'.format(r.choice(e.NUMS[2:]), r.choice(e.NUMS))
    return '{}So when will we moon? Exactly on this date {} {}  {} / {} / {}'.format(
            e.CHART_UP, clock, e.ARR_RIGHT, day, month, year)


async def do_announce(msg, title, client):
    announcement = discord.Embed(title=title, colour=discord.Colour.red(), description=msg)
    chans = await q.get_main_chans()
    for chanID in chans:
        try:
            chan = client.get_channel(int(chanID))
            await chan.send(embed=announcement)
        except Exception:
            pass


async def dm_user(authr, msg_embed, msg_on_fail=False):
    # The case that authr is a message context
    if not isinstance(authr, discord.Member):
        ctx = authr
        authr = authr.author
    if authr.dm_channel is None:
        await authr.create_dm()
    try:
        await authr.send(embed=msg_embed)
        return True
    except discord.errors.Forbidden:
        if msg_on_fail:
            await ctx.send(docs.rule_fail_send)
        return False


def check_times(user_obj):
    ctime = round(time.time())
    return '''
Faucet: {}
Withdrawals: {}
Donations: {}
'''.format(e.CANNOT[:-3] + ' ({})'.format(time_display(ceil(user_obj.next_fct()-ctime))) if not user_obj.can_faucet() else e.GOOD[:-3],
            e.CANNOT[:-3] + ' ({})'.format(time_display(ceil(user_obj.next_net_tx()-ctime))) if not user_obj.can_net_tx() else e.GOOD[:-3],
            e.CANNOT[:-3] + ' ({})'.format(time_display(ceil(user_obj.next_donate()-ctime))) if not user_obj.can_donate() else e.GOOD[:-3])



# ADMIN COMMANDS
def verify_pass(pwd):
    if g.admin_pass_hash != '':
        return sha512(pwd.encode()).hexdigest() == g.admin_pass_hash
    return True


async def blist_iface(args, blist_obj, password=''):
    if not verify_pass(password): return docs.failed_auth
    if args[0] == 'ban':
        if len(args) == 3:
            await blist_obj.new_blist(args[1], True if args[2] == 'priv' else False)
            return e.GOOD[:-3]
        return f'{e.ERROR}Invalid args'
    elif args[0] == 'unban':
        if len(args) == 2:
            await blist_obj.remove_blist(args[1])
            return e.GOOD[:-3]
        return f'{e.ERROR}Invalid args'
    else:
        return f'{e.ERROR}Invalid command'


async def burn_coins(args, password=''):
    if not verify_pass(password): return docs.failed_auth
    if (amt_filter(args[1]) is None):
        amt = 0
    else:
        amt = amt_filter(args[1])
    from_user = await q.get_user(args[0])
    if args[0] != g.owner_id and (not from_user is None):
        to_user = await q.get_user(g.owner_id)
        await from_user.send_internal_tx(to_user, amt)
    else:
        from_user.balance -= amt
        await q.save_user(from_user)
    return f'Burned `{amt} GRC` from `{args[0]}`'


async def toggle_wallet_lock(password=''):
    if not verify_pass(password): return docs.failed_auth
    if await w.is_unlocked():
        await w.query('walletlock', [])
        return f'{e.GOOD}Wallet has been locked.'
    await w.unlock()
    return f'{e.GOOD}Wallet has been unlocked.'


def toggle_deposits(password=''):
    if not verify_pass(password): return docs.failed_auth
    if w.deposits_online():
        with open(g.LST_BLK+'.bak', 'w') as bak_last_block_file:
            bak_last_block_file.write(str(w.get_block_search_num()))
        with open(g.LST_BLK, 'w') as last_block_file:
            last_block_file.write(str(g.LOCK_HEIGHT))
        return f'{e.GOOD}Deposits locked.'
    elif path.exists(g.LST_BLK+'.bak'):
        with open(g.LST_BLK, 'w') as last_block_file:
            last_block_file.write(str(w.get_block_search_num(ext='.bak')))
        return f'{e.GOOD}Deposits unlocked.'
    else:
        return f'{e.CANNOT}Scavenger not running.'


async def user_stats(uid):
    user = await q.get_user(uid)
    if user is None:
        return discord.Embed(title='Unknown user', colour=discord.Colour.red())
    return discord.Embed(title='User stats',
                        description=f'''User ID: `{uid}`
Balance: `{user.balance} GRC`
Stakes: `{user.stakes} GRC`
Donations: `{user.donations} GRC`
Address: `{user.address}`
Last tx: `{user.active_tx[2]}`
''')
