from time import time
import logging
import aiohttp
import asyncio
import json

class price_bot:
    last_updated = 0
    last_price = {'usd' : 0, 'btc' : 0}
    timeout_sec = 380
    price_url = 'https://api.coingecko.com/api/v3/coins/gridcoin-research'

    async def price(self, btc=False):
        if btc:
            index = 'btc'
        else:
            index = 'usd'
        if round(time()) > self.last_updated+self.timeout_sec:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get(self.price_url) as response:
                        data = json.loads(await response.text())
                        self.last_price['usd'] = data['market_data']['current_price']['usd']
                        self.last_price['btc'] = data['market_data']['current_price']['btc']
            except Exception as E:
                logging.warning('[WARN] Error when trying to fetch USD price: %s', E)
                return self.last_price[index]
            self.last_updated = round(time())
        return self.last_price[index]

    async def conv(self, amt, btc=False):
        return '{:.2f}'.format(amt*(await self.price(btc)))
