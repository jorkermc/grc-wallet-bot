import aiohttp
import logging
import json
from re import finditer
from datetime import datetime
from os import path

import grcconf as g

'''
0 - protocol/client error
2 - invalid args
3 - net exception
4 - invalid type
'''


def scan_projects(averages: str) -> dict:
    final = {}
    for project_info in averages.split(';')[:-1]:
        project, team_rac, __ = project_info.split(',')
        if project != 'NeuralNetwork':
            final[project] = team_rac + ' RAC'
    return final


async def query(cmd, params):
    if not all([isinstance(cmd, str), isinstance(params, list)]):
        logging.warning('Invalid data sent to wallet query')
        return 2
    command = json.dumps({'method' : cmd, 'params' : params})
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(g.rpc_url, data=command, headers={'content-type': "application/json", 'cache-control': "no-cache"}, auth=aiohttp.BasicAuth(g.rpc_usr, password=g.rpc_pass)) as resp:
                # split up steps as a workaround for resp.json() not working
                # with output from `gettransaction` for superblock transactions
                # (aiohttp trying to decode <BINARY> part in hashboinc string)
                raw = await resp.read()
                text = raw.decode('utf-8', 'ignore')
                response = json.loads(text)
    except Exception as E:
        logging.warning('Exception triggered in communication with GRC client: %s', E)
        logging.warning('CMD: %s ARGS: %s', cmd, params)
        return 3
    if response['error'] != None:
        if response['error']['code'] == -17:
            return -17
        if response['error']['code'] == -5:
            return -5
        logging.warning('Error response sent by GRC client: %s', response['error'])
        logging.warning('CMD: %s ARGS: %s', cmd, params)
        return 0
    else:
        return response['result']


async def tx(addr, amount, comment=None):
    if isinstance(addr, str) and len(addr) > 1:
        args = [addr, amount]
        if comment:
            args.append(comment)
        return await query('sendtoaddress', args)
    return 4


async def get_block(height=None):
    current_block = await query('getblockcount', [])
    if height is None:
        height = current_block
    if height < 0 or height > current_block:
        return None
    else:
        data = {}
        block_data = await query('getblockbynumber', [height])
        if type(block_data) is int:
            return None
        data = {
            'Height': height,
            'Hash': block_data['hash'],
            'Time (UTC)': datetime.utcfromtimestamp(block_data['time']).isoformat(' '),
            'Difficulty': round(block_data['difficulty'], 4),
            'Net Weight': round(g.NET_W_MULT * block_data['difficulty']),
            'No. of TXs': len(block_data['tx']),
            'Amount Minted': block_data['mint'],
            'Superblock': 'No' if (block_data['IsSuperBlock'] == 0) else 'Yes',
        }
        return data


async def get_last_superblock():
    listdata_sb = await query('listdata', ['superblock'])
    height = listdata_sb['block_number']
    sb_details = await query('showblock', [int(height)])
    sb_time = datetime.utcfromtimestamp(sb_details['time']).isoformat(' ')
    final = {'Height' : height, 'Time (UTC)' : sb_time}
    final.update(scan_projects(listdata_sb['averages']))
    return final


async def get_latest_stakes():
    data = await query('listtransactions', ['', g.tx_lookback])
    stakes = {}
    for tx in data:
        if (tx['category'] == 'generate') and (tx.get('Type', None) == 'POS'):
            stakes[tx['txid']] = g.STK_REWARD
    return stakes


async def isConfirmed(txid):
    tx_data = await query('gettransaction', [txid])
    if isinstance(tx_data, int):
        return tx_data
    return tx_data['confirmations'] > g.tx_timeout


async def unlock():
    result = await query('walletpassphrase', [g.grc_pass, 999999999, False])
    if result is None:
        return True
    return result


async def is_unlocked():
    result = await unlock()
    if result is True:
        await query('walletlock', [])
        return False
    return True


def get_block_search_num(ext=''):
    with open(g.LST_BLK+ext, 'r') as last_block_file:
        height = int(last_block_file.read().split()[0])
    return height


def deposits_online():
    if path.exists(g.LST_BLK):
        return get_block_search_num() != g.LOCK_HEIGHT
    return False


async def get_version():
    data = await query('getinfo', [])
    return data['version']
