import aiomysql
from time import time

from user import User
import grcconf as g
import wallet as w


# Time constants
H1 = 60*60
H24 = 24*H1


def bit_to_bool(bit):
    return int.from_bytes(bit, 'big') == 0


async def uid_exists(uid):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT uid FROM {g.db_name}.udb WHERE uid=%s;', (uid,))
    response = await c.fetchall()
    db.close()
    return len(response) > 0


async def get_user(uid):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    #Get user record from udb
    await c.execute(f'SELECT * FROM {g.db_name}.udb WHERE uid=%s;', (uid,))
    usr_record = await c.fetchall()
    #Get unconfirmed balance
    await c.execute(f'SELECT sum(amount) FROM {g.db_name}.deposits WHERE uid=%s AND confirmed=0;', (uid,))
    unconfirmed_bal = await c.fetchone()
    db.close()
    if len(usr_record) == 0: return None;
    usr_record = usr_record[0]
    return User(uid, address=usr_record[1], last_faucet=usr_record[2], balance=usr_record[3],
                donations=usr_record[4], lastTX=[usr_record[5], usr_record[6], usr_record[7]],
                dm_enable=usr_record[8], stakes=usr_record[9], unconfirmed=unconfirmed_bal[0])


async def get_bal(uid):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT balance, address FROM {g.db_name}.udb WHERE uid=%s;', (uid,))
    result = await c.fetchone()
    db.close()
    return result


async def get_unconfirmed(uid):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT sum(amount) FROM {g.db_name}.deposits WHERE uid=%s AND confirmed=0;', (uid,))
    unconfirmed_bal = (await c.fetchone())[0]
    if unconfirmed_bal is None:
        unconfirmed_bal = 0
    db.close()
    return unconfirmed_bal


async def register_deposit(txid, amount, uid):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT count(txid) FROM {g.db_name}.deposits WHERE txid=%s AND uid=%s;', (txid, uid))
    unique = (await c.fetchone())[0] == 0
    if unique:
        await c.execute(f'INSERT INTO {g.db_name}.deposits VALUES (%s, %s, %s, DEFAULT);',
                        (txid, amount, uid))
        await db.commit()
    db.close()
    return unique


async def check_deposits():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT txid, amount, uid FROM {g.db_name}.deposits WHERE confirmed=0;')
    all_unconfirmed_deposits = await c.fetchall()
    for deposit in all_unconfirmed_deposits:
        txid = deposit[0]
        amount = deposit[1]
        uid = deposit[2]
        # Incase daemon uses -1 sentinel value for confirmations
        tx_confirms = await w.query('gettransaction', [txid])
        if isinstance(tx_confirms, int):
            tx_confirms = None
        else:
            tx_confirms = tx_confirms['confirmations']
        tx_confirmed = await w.isConfirmed(txid)
        if tx_confirmed == -5 or tx_confirms == -1: # Orphaned transaction
            await c.execute(f'DELETE FROM {g.db_name}.deposits WHERE txid=%s;', (txid,))
        elif tx_confirmed is True:
            await c.execute(f'UPDATE {g.db_name}.deposits SET confirmed=1 WHERE txid=%s;', (txid,))
            await c.execute(f'UPDATE {g.db_name}.udb SET balance=balance+%s WHERE uid=%s;', (amount, uid))
    await db.commit()
    db.close()


async def register_stake(txid, amount):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT count(txid) FROM {g.db_name}.stakes WHERE txid=%s;', (txid,))
    unique = (await c.fetchone())[0] == 0
    if unique:
        data = await w.query('gettransaction', [txid])
        await c.execute(f'INSERT INTO {g.db_name}.stakes VALUES (%s, %s, %s);', (txid, amount, data['time']))
        await db.commit()
    db.close()
    return unique


async def get_last_stake():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT * FROM {g.db_name}.stakes ORDER BY time DESC LIMIT 1;')
    result = await c.fetchone()
    result_dict = {'txid' : result[0], 'amount' : result[1], 'time' : result[2]}
    db.close()
    return result_dict


async def get_num_stakes():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT count(txid) FROM {g.db_name}.stakes WHERE time > 0;')
    result = await c.fetchone()
    db.close()
    return result[0]


async def log_transaction(sender, receiver, amount):
    if all([not isinstance(p, str) for p in [sender, receiver]]):
        sender = sender.usrID
        receiver = receiver.usrID
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'INSERT INTO {g.db_name}.tx_log VALUES (%s, %s, %s, %s);',
                (sender, receiver, amount, int(time())))
    await db.commit()
    db.close()


async def get_24h_tx_volume():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT sum(amount) FROM {g.db_name}.tx_log WHERE time > %s;', (time() - H24,))
    result = await c.fetchone()
    db.close()
    return result[0]


async def get_total_donations():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT sum(donations) FROM {g.db_name}.udb;')
    result = await c.fetchone()
    db.close()
    return result[0]


async def get_top_10_donators():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT uid, donations FROM {g.db_name}.udb ORDER BY donations DESC LIMIT 10;')
    results = await c.fetchall()
    db.close()
    return results


async def get_total_creditor_balance():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT sum(balance) FROM {g.db_name}.udb;')
    result = await c.fetchone()
    db.close()
    return result[0]


async def save_user(user_objs):
    if not isinstance(user_objs, list): user_objs = [user_objs];
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    for user in user_objs:
        await c.execute(f'''UPDATE {g.db_name}.udb SET
            last_faucet=%s,
            balance=%s,
            donations=%s,
            lastTX_amt=%s,
            lastTX_time=%s,
            lastTX_txid=%s
            WHERE uid=%s;''',
            (user.last_faucet, user.balance, user.donations,
            user.active_tx[0], user.active_tx[1], user.active_tx[2],
            user.usrID))
    await db.commit()
    db.close()


async def new_user(uid, address):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'INSERT INTO {g.db_name}.udb VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);',
                    (uid, address, int(time() + (g.NEW_USR_TIME-1)*H24-g.FCT_REQ_LIM*H1), 0, 0, 0, 0, '', 0, 0))
    await db.commit()
    db.close()


async def get_addr_uid_dict(dm_enables=False):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    if dm_enables:
        await c.execute(f'SELECT address, uid, dm_enable FROM {g.db_name}.udb;')
        user_data = {}
        enables_data = {}
        for tup in await c.fetchall():
            user_data[tup[0]] = tup[1]
            enables_data[tup[1]] = bit_to_bool(tup[2])
        db.close()
        return user_data, enables_data
    else:
        await c.execute(f'SELECT address, uid FROM {g.db_name}.udb;')
        user_data = {}
        for tup in await c.fetchall():
            user_data[tup[0]] = tup[1]
        db.close()
        return user_data


async def apply_balance_changes(user_vals, is_stake=False):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    for uid in user_vals:
        if is_stake:
            await c.execute(f'UPDATE {g.db_name}.udb SET balance = balance + %s, stakes = stakes + %s WHERE uid=%s;',
                            (user_vals[uid], user_vals[uid], uid))
        else:
            await c.execute(f'UPDATE {g.db_name}.udb SET balance = balance + %s WHERE uid=%s;',
                            (user_vals[uid], uid))
    await db.commit()
    db.close()


async def get_max_bal():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT max(balance) FROM {g.db_name}.udb;')
    result = await c.fetchone()
    db.close()
    return result[0]


async def get_blacklisted():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT uid, channel FROM {g.db_name}.blacklist;')
    blacklisted = {}
    for tup in await c.fetchall():
        channel = int.from_bytes(tup[1], 'big')
        blacklisted[tup[0]] = True if channel == 1 else False
    db.close()
    return blacklisted


async def commit_ban(user, channel):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'INSERT INTO {g.db_name}.blacklist VALUES (%s, %s);', (user, channel))
    await db.commit()
    db.close()


async def commit_unban(user):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'DELETE FROM {g.db_name}.blacklist WHERE uid=%s;', (user))
    await db.commit()
    db.close()


async def add_to_fee_pool(fee):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT amount FROM {g.db_name}.admin_claims WHERE txid="PENDING";')
    current_owed = (await c.fetchone())[0] + fee
    await c.execute(f'UPDATE {g.db_name}.admin_claims SET amount = %s WHERE txid="PENDING";', (current_owed))
    if current_owed > g.FEE_WDR_THRESH:
        txid = await w.tx(g.admin_wallet, current_owed)
        if isinstance(txid, str):
            await c.execute(f'UPDATE {g.db_name}.admin_claims SET txid = %s WHERE txid="PENDING";', (txid,))
            await c.execute(f'INSERT INTO {g.db_name}.admin_claims VALUES (%s, %s);', ('PENDING', 0))
    await db.commit()
    db.close()


async def get_main_chans():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT * FROM {g.db_name}.channels;')
    results = {}
    for chan in await c.fetchall():
        results[chan[0]] = bit_to_bool(chan[1])
    db.close()
    return results


async def add_chan(chanID):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT count(channel) FROM {g.db_name}.channels WHERE channel=%s;', (chanID))
    chan_exists = (await c.fetchone())[0] > 0
    if chan_exists:
        await c.execute(f'DELETE FROM {g.db_name}.channels WHERE channel=%s;', (chanID))
    else:
        await c.execute(f'INSERT INTO {g.db_name}.channels VALUES (%s, 1);', (chanID))
    await db.commit()
    db.close()
    return not chan_exists


async def get_donors():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT * FROM {g.db_name}.donors ORDER BY name;')
    final = []
    for tup in await c.fetchall():
        final.append({tup[0] : tup[1]})
    db.close()
    return final


async def toggle_dms(uid):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT dm_enable FROM {g.db_name}.udb WHERE uid=%s;', (uid,))
    result = await c.fetchone()
    out = bit_to_bool(result[0])
    if out:
        bit = b'\x01'
    else:
        bit = b'\x00'
    await c.execute(f'UPDATE {g.db_name}.udb SET dm_enable=%s WHERE uid=%s;', (bit, uid))
    await db.commit()
    db.close()
    return not out


async def get_all_bals():
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT uid, balance FROM {g.db_name}.udb;')
    result = await c.fetchall()
    db.close()
    return result


async def get_stakes(uid):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT stakes FROM {g.db_name}.udb WHERE uid=%s;', (uid,))
    result = await c.fetchone()
    db.close()
    return result[0]


async def get_faucet_advert(i):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT message FROM {g.db_name}.adverts WHERE active=1;')
    results = await c.fetchall()
    db.close()
    return results[i % len(results)][0]


async def add_new_advert(email, text):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'INSERT INTO {g.db_name}.adverts VALUES (NULL, %s, %s, 1);', (text, email))
    await db.commit()
    db.close()


async def delist_advert(email):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'UPDATE {g.db_name}.adverts SET active=0 WHERE ref_email=%s;', (email))
    await db.commit()
    db.close()


async def addr_exists(addr):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT count(address) FROM {g.db_name}.udb WHERE address=%s;', (addr))
    result = await c.fetchone()
    db.close()
    return result[0]


async def faucet_ban(uid, ban_time):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    next_faucet = round(time()) + ban_time
    await c.execute(f'UPDATE {g.db_name}.udb SET last_faucet=%s WHERE uid=%s;', (next_faucet, uid))
    await db.commit()
    db.close()


# Highly redundant but efficient faucet operation
async def faucet_operations(uid, amount, ctime):
    db = await aiomysql.connect(host=g.sql_db_host, user=g.sql_db_usr, password=g.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT * FROM {g.db_name}.udb WHERE uid=%s;', (uid,))
    result = (await c.fetchall())[0]
    user_obj = User(uid, address=result[1], last_faucet=result[2], balance=result[3],
                donations=result[4], lastTX=[result[5], result[6], result[7]])
    if not user_obj.can_faucet():
        db.close()
        return user_obj, 1

    await c.execute(f'SELECT balance FROM {g.db_name}.udb WHERE uid=%s;', ('FAUCET'))
    fct_bal = (await c.fetchone())[0]
    if fct_bal < g.FCT_MAX:
        db.close()
        return None, 2

    user_vals = {'FAUCET' : -amount, uid : amount}
    for uid in user_vals:
        await c.execute(f'UPDATE {g.db_name}.udb SET balance = balance + %s WHERE uid=%s;',
                        (user_vals[uid], uid))
    await c.execute(f'UPDATE {g.db_name}.udb SET last_faucet = %s WHERE uid=%s;', (ctime, uid))
    await db.commit()
    db.close()
    user_obj.balance += amount
    return user_obj, 0
