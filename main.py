import asyncio
import logging
from time import time, mktime, strptime, perf_counter
from logging.handlers import RotatingFileHandler
from datetime import datetime, timedelta
from random import choice as rchoice

import discord
from discord.ext import commands

import extras
import docs
import emotes as e
import errors
import grcconf as g
import wallet as w
import queries as q
import help_docs
from grc_pricebot import price_bot
from blacklist import Blacklister
from rain_bot import Rainbot

# Set up logging functionality
handler = [RotatingFileHandler(g.log_dir+'walletbot.log', maxBytes=10**7, backupCount=3)]
logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s',
                    datefmt='%d/%m %T',
                    level=logging.INFO,
                    handlers=handler)

client = commands.Bot(command_prefix=g.pre)
client.remove_command('help')
FCT = 'FAUCET'
RN = 'RAIN'
latest_users = {}
blacklister = None
price_fetcher = price_bot()
rbot = None
main_chans = {}


### GENERAL FUNCTIONS
def checkspam(user):
    global latest_users
    ctime = time()
    if user in latest_users:
        if ctime - latest_users[user] > 1:
            latest_users[user] = ctime
            return False
        else:
            latest_users[user] = ctime
            return True
    else:
        latest_users[user] = ctime
        return False


async def check_rain(ctx):
    if await rbot.can_rain():
        await ctx.send(docs.rain_feedback)
        await rbot.do_rain(client)
    elif rbot.balance > rbot.thresh:
        await ctx.send(docs.rain_wait)
###


### DECORATORS
def in_udb():
    async def predicate(ctx):
        if not await q.uid_exists(str(ctx.author.id)):
            raise errors.NotInUDB()
        return True
    return commands.check(predicate)


def limit_to_main_channel():
    def predicate(ctx):
        if not (str(ctx.channel.id) in main_chans or isinstance(ctx.channel, discord.DMChannel)):
            raise errors.LimChannel()
        return True
    return commands.check(predicate)


def is_owner():
    def predicate(ctx):
        return str(ctx.author.id) == g.owner_id
    return commands.check(predicate)
###


@client.event
async def on_command_error(ctx, error):
    if hasattr(ctx.command, 'on_error') or isinstance(error, discord.Forbidden):
        return
    if isinstance(error, commands.CommandNotFound):
        return await ctx.send(f'{e.INFO}Invalid command. Type `%help` for help.')
    if isinstance(error, errors.NotInUDB):
        return await ctx.send(f'{e.ERROR}You do not have an account. (type `%new` to register or type `%help` for help)')
    if isinstance(error, errors.LimChannel):
        try:
            assigned_channel = (set(main_chans) & {*map(lambda x: str(x.id), ctx.guild.channels)}).pop()
            if ctx.command.name == 'faucet' and main_chans[str(assigned_channel)]:
                await q.faucet_ban(str(ctx.author.id), g.auto_ban_time)
                logging.info('User %s has been banned.', ctx.author.id)
                return await extras.dm_user(ctx.author, docs.banned)
            else:
                return await ctx.send(docs.change_channel.format(assigned_channel))
        except KeyError:
            return await ctx.send(docs.no_default_channel)
    if isinstance(error, (commands.MissingRequiredArgument, commands.BadArgument)):
        if ctx.command.name == 'withdraw':
            return await ctx.send(f'{e.INFO}To withdraw from your account type: `%wdr [address to send to] [amount-GRC]`\nA service fee of {g.tx_fee} GRC is subtracted from what you send. If you wish to send GRC to someone in the server, use `%give`')
        if ctx.command.name == 'donate':
            return await ctx.send(extras.index_displayer(f'{e.GIVE}Be generous! Below are possible donation options.\nTo donate, type `%donate [selection no.] [amount-GRC]`\n', await q.get_donors()))
        if ctx.command.name == 'rdonate':
            return await ctx.send(f'{e.GIVE}To donate to a random contributor type: `%rdonate [amount-GRC]`')
        if ctx.command.name == 'give':
            return await ctx.send(f'{e.INFO}To give funds to a member in the server, type `%give [discord mention of user] [amount to give]`.\nThe person must also have an account with the bot.')
        if ctx.command.name == 'fgive':
            return await ctx.send(f'{e.ERROR}Please specify an amount to give.')
        if ctx.command.name == 'rain':
            await ctx.send(await rbot.status())
            return await check_rain(ctx)
        if ctx.command.name == 'rainhere':
            return await ctx.send(f'{e.INFO}To rain on this server, type `%rainhere [amount-GRC]`')
        if ctx.command.name == 'faq':
            if await extras.dm_user(ctx, extras.faq_index(), msg_on_fail=True):
                await ctx.message.add_reaction(e.WHITE_CHECK)
            return
        if ctx.command.name == 'block':
            return await ctx.send(await extras.show_block(await w.query('getblockcount', [])))
        if ctx.command.name == 'help':
            return await ctx.send(embed=help_docs.help_main())
        if ctx.command.name == 'quotestake':
            return await ctx.send(docs.invalid_val)
        if ctx.command.name == 'showgrid':
            return await ctx.send(docs.unimplemented)
            return await ctx.send(embed=await extras.fetch_grids())
        if ctx.command.name == 'gridplace':
            return await ctx.send(docs.unimplemented)
            return await ctx.send(docs.gp_help)
    if isinstance(error, commands.NoPrivateMessage):
        return await ctx.send(docs.pm_restrict)
    print(error)


@client.event
async def on_ready():
    global blacklister, rbot, main_chans
    if hasattr(client, 'initialised'):
        return  # Prevents multiple on_ready calls

    if await w.query('getblockcount', []) > 5:  # 5 is largest error return value
        logging.info('Gridcoin client is online')
    else:
        logging.error('GRC client is not online')
        await client.logout()
        return

    if not await q.uid_exists(FCT):
        logging.error('Could not connect to SQL database')
        await client.logout()
        return
    else:
        logging.info('SQL DB online and accessible')

    try:
        blacklister = Blacklister(await q.get_blacklisted())
        logging.info('Blacklisting service loaded correctly')
    except Exception:
        logging.error('Blacklisting service failed to load')
        await client.logout()
        return

    if await w.unlock():
        logging.info('Wallet successfully unlocked')
    else:
        logging.error('There was a problem trying to unlock the gridcoin wallet')
        await client.logout()
        return

    try:
        rbot = Rainbot()
        await rbot.get_balance()
        logging.info('Rainbot service loaded correctly')
    except Exception as E:
        logging.error('Rainbot service failed to load: %s', E)
        await client.logout()
        return

    try:
        main_chans = await q.get_main_chans()
        logging.info('Loaded main channels:' + ''.join([f'\n{c}' for c in main_chans]))
    except Exception as E:
        logging.error('Failed to load main channels: %s', E)

    client.initialised = True
    logging.info('Initialisation complete')


@client.command()
@limit_to_main_channel()
async def status(ctx):
    await ctx.send(embed=await extras.dump_cfg())


@client.command(aliases=['New', 'NEW', 'register', 'Register', 'reg', 'Reg', 'REG'])
async def new(ctx):
    if not await q.uid_exists(str(ctx.author.id)):
        await ctx.send(docs.welcome)
        try:
            addr = await w.query('getnewaddress', [])
            await q.new_user(str(ctx.author.id), addr)
            await ctx.send(docs.new_user_success.format(e.GOOD, addr))
        except Exception as E:
            logging.error('Could not create new user for %s. Error: %s', ctx.author.id, E)
            return await ctx.send(docs.new_user_fail)

        await extras.dm_user(ctx, docs.rules, msg_on_fail=True)
        await extras.dm_user(ctx, docs.terms, msg_on_fail=True)
    else:
        await ctx.send(docs.already_user)


@client.command(name='help', aliases=['Help', 'h', 'H'])
@limit_to_main_channel()
async def _help(ctx, command):  # Not overwriting the built-in help function
    # Look up the command in case it is an alias
    lookup_command = client.get_command(command.replace('[', '').replace(']', '').lower())
    if lookup_command is not None:
        command = lookup_command.name
    await ctx.send(embed=extras.help_interface(command))


@client.command(aliases=['Info'])
@limit_to_main_channel()
async def info(ctx):
    await ctx.send(embed=docs.info)


@client.command(aliases=['Faq', 'FAQ'])
async def faq(ctx, query: int):
    reply = extras.faq(query)
    if isinstance(reply, str):
        await ctx.send(reply)
    elif await extras.dm_user(ctx, reply, msg_on_fail=True):
        await ctx.message.add_reaction(e.WHITE_CHECK)


@client.command(aliases=['blk', 'Block'])
async def block(ctx, query: int):
    await ctx.send(await extras.show_block(query))


@client.command(aliases=['sb', 'sblock', 'Sblock', 'Sb'])
async def superblock(ctx):
    await ctx.send(await extras.show_superblock())


@client.command(aliases=['Rules', 'rule', 'Rule'])
async def rules(ctx):
    if await extras.dm_user(ctx, docs.rules, msg_on_fail=True):
        await ctx.message.add_reaction(e.WHITE_CHECK)


@client.command(aliases=['Terms', 'term', 'Term'])
async def terms(ctx):
    if await extras.dm_user(ctx, docs.terms, msg_on_fail=True):
        await ctx.message.add_reaction(e.WHITE_CHECK)


@client.command(aliases=['bal', 'Bal', 'BAL', 'b', 'B', 'Balance'])
@in_udb()
async def balance(ctx):
    uid = str(ctx.author.id)
    bal = await q.get_bal(uid)
    unconfirmed = await q.get_unconfirmed(uid)
    await ctx.send(docs.balance_template.format(
        e.BAL, bal[1], '{:.8f}'.format(abs(bal[0])),
        await price_fetcher.conv(abs(bal[0])),
        '{:.8f}'.format(unconfirmed))
    )


@client.command(aliases=['addr', 'Addr', 'deposit', 'Deposit', 'a', 'A'])
@in_udb()
@limit_to_main_channel()
async def address(ctx):
    await ctx.send(docs.min_deposit)
    await ctx.send((await q.get_bal(str(ctx.author.id)))[1])


@client.command(aliases=['wdr', 'WDR', 'send', 'Send', 'SEND', 'Withdraw'])
@in_udb()
@limit_to_main_channel()
async def withdraw(ctx, address: str, amount: float):
    if await w.is_unlocked():
        if not await q.addr_exists(address):
            user_obj = await q.get_user(str(ctx.author.id))
            return await ctx.send(await user_obj.withdraw(extras.amt_filter(amount), address, g.tx_fee))
        return await ctx.send(docs.address_warning)
    await ctx.send(docs.withdrawals_offline)


@client.command(aliases=['d', 'D', 'Donate'])
@in_udb()
async def donate(ctx, selection: int, amount: float):
    if await w.is_unlocked():
        user_obj = await q.get_user(str(ctx.author.id))
        return await ctx.send(await extras.donate(user_obj, selection, extras.amt_filter(amount)))
    await ctx.send(docs.withdrawals_offline)


@client.command(aliases=['donors', 'Donors', 'Leaderboard'])
@limit_to_main_channel()
async def leaderboard(ctx):
    await ctx.send(embed=await extras.show_top_10_donators(client))


@client.command(aliases=['rd', 'Rd', 'RD', 'Rdonate'])
@in_udb()
async def rdonate(ctx, amount: float):
    if await w.is_unlocked():
        user_obj = await q.get_user(str(ctx.author.id))
        return await ctx.send(await extras.rdonate(user_obj, extras.amt_filter(amount)))
    await ctx.send(docs.withdrawals_offline)


@client.command(aliases=['tip', 'Tip', 'TIP', 'Give'])
@in_udb()
async def give(ctx, receiver: discord.User, amount: float):
    if str(receiver.id) == str(ctx.author.id):
        return await ctx.send(docs.cannot_send_self)
    sender_obj = await q.get_user(str(ctx.author.id))
    receiver_obj = await q.get_user(str(receiver.id))
    if receiver_obj is None:
        return await ctx.send(f'{e.ERROR}Invalid user specified.')
    await ctx.send(await sender_obj.send_internal_tx(receiver_obj, extras.amt_filter(amount)))


@client.command(aliases=['faucetgive', 'Faucetgive', 'fctgive', 'Fctgive', 'Fgive'])
@in_udb()
async def fgive(ctx, amount: float):
    user_obj = await q.get_user(str(ctx.author.id))
    result = await user_obj.send_internal_tx(await q.get_user(FCT), extras.amt_filter(amount), True)
    await ctx.send(result)
    if result.startswith(e.GOOD):
        await ctx.send(docs.faucet_thankyou)


@client.command(aliases=['fct', 'Fct', 'FCT', 'get', 'Get', 'GET', 'Faucet'])
@commands.guild_only()
@in_udb()
@limit_to_main_channel()
async def faucet(ctx):
    await ctx.send(await extras.faucet(str(ctx.author.id)))


@client.command(aliases=['rn', 'Rn', 'RN', 'Rain', 'RAIN'])
@in_udb()
async def rain(ctx, amount: float):
    user_obj = await q.get_user(str(ctx.author.id))
    await ctx.send(await rbot.contribute(extras.amt_filter(amount), user_obj))
    await check_rain(ctx)


@client.command(aliases=['rh', 'Rh', 'RH', 'Rainhere'])
@commands.guild_only()
@in_udb()
async def rainhere(ctx, amount: float):
    amount = extras.amt_filter(amount)
    user_obj = await q.get_user(str(ctx.author.id))

    if amount is None:
        return await ctx.send(docs.invalid_val)
    if amount < g.MIN_RAIN:
        return await ctx.send(docs.rain_below_min)
    if user_obj.balance >= amount:
        await ctx.send(docs.rain_feedback)
        user_obj.balance -= amount
        user_obj.donations += amount
        await q.save_user(user_obj)
        resp = await rbot.do_rain(client, server=str(ctx.guild.id), local_amount=amount)
        if type(resp) is int:
            num_users = resp
        else:
            return await ctx.send(resp)
        if num_users is None:
            return await ctx.send(docs.rain_wait)
        rain_em = discord.Embed(title=docs.rain_title, description=docs.rain_announce_msg.format(amount, num_users), colour=discord.Colour.red())
        return await ctx.send(embed=rain_em)

    await ctx.send(docs.insufficient_funds)


@client.command(aliases=['DM', 'Dm', 'messages', 'Messages'])
@in_udb()
@limit_to_main_channel()
async def dm(ctx):
    if await q.toggle_dms(str(ctx.author.id)):
        await ctx.send(docs.dm_enabled)
    else:
        await ctx.send(docs.dm_disabled)


@client.command(aliases=['Stake', 'stk', 'Stk', 'STK', 'steak', 'Steak'])
@in_udb()
@limit_to_main_channel()
async def stake(ctx):
    await ctx.send(docs.stake_template.format('{:.8f}'.format(await q.get_stakes(str(ctx.author.id)))))


@client.command(aliases=['Quotestake', 'quotesteak', 'Quotesteak', 'quotestake', 'Quote', 'stkest', 'Stkest', 'stakereport', 'Stakereport', 'steakreport', 'est', 'Est', 'EST'])
async def quote(ctx, amount: float):
    return await ctx.send(await extras.stake_calculator(amount))


@client.command(aliases=['statistic', 'stats', 'users'])
async def statistics(ctx):
    return await ctx.send(embed=await extras.get_viewer_statistics(client))

@client.command(aliases=['Showgrid', 'showgame', 'Showgrids', 'showgrids', 'showgames', 'Showgames', 'grids', 'Grids', 'grid', 'Grid', 'game', 'Game', 'games', 'Games'])
@limit_to_main_channel()
async def showgrid(ctx, name):
    return await ctx.send(docs.unimplemented)
    await ctx.send(embed=await extras.fetch_grid(' '.join(filter(lambda x: not x.startswith('%'), ctx.message.content.split())).lower()))


@client.command(aliases=['Gridplace', 'place', 'Place', 'color', 'Color', 'colour', 'Colour'])
@in_udb()
@limit_to_main_channel()
async def gridplace(ctx, name: str, x: int, y: int, colour: str):
    return await ctx.send(docs.unimplemented)
    user_obj = await q.get_user(str(ctx.author.id))
    await ctx.send(await extras.gp_place(name.lower(), x, y, colour, user_obj))


@client.command(aliases=['Qr', 'QR'])
@commands.guild_only()
@limit_to_main_channel()
@in_udb()
async def qr(ctx, text=None):
    if text is None:
        addr = (await q.get_bal(str(ctx.author.id)))[1]
        return await ctx.send(file=discord.File(extras.get_qr(addr), filename=f'{ctx.author.name}.png'))
    await ctx.send(file=discord.File(extras.get_qr(text), filename=f'{ctx.author.name}.png'))


@client.command(name='time', aliases=['Time', 'TIME', 't', 'T'])
@in_udb()
@limit_to_main_channel()
async def _time(ctx):
    user_obj = await q.get_user(str(ctx.author.id))
    await ctx.send(extras.check_times(user_obj))


@client.command(aliases=['grcmoon', 'whenmoon', 'lambo', 'whenlambo', 'coffee'])
async def moon(ctx):
    await ctx.send(extras.moon())


@client.command(aliases=['Take', 'steal', 'Steal'])
async def take(ctx):
    await ctx.send(rchoice(docs.take_msgs))


@client.command(aliases=['Price', 'p', 'P'])
@limit_to_main_channel()
async def price(ctx):
    await ctx.send(f'''```
GRC price (USD): ${round(await price_fetcher.price(), 4)}
GRC price (BTC): ₿{"{:.8f}".format(await price_fetcher.price(btc=True), 8)}
```''')


@client.command(aliases=['conv'])
@limit_to_main_channel()
async def convert(ctx: commands.Context, _amount: float):
    amount = extras.amt_filter(_amount)

    if amount:
        await ctx.send(f'''```
        {amount} GRC = ${await price_fetcher.conv(amount):.4f} = ₿{await price_fetcher.conv(amount, btc=True):.8f}
```''')
    else:
        await ctx.message.add_reaction("\u274C")


@client.command(aliases=['Ping', 'PING'])
async def ping(ctx):
    t1 = perf_counter()
    await ctx.trigger_typing()
    t2 = perf_counter()
    time_delta = round((t2 - t1) * 1000)
    await ctx.send(f"`{time_delta}ms`")


@client.command(aliases=['i', 'I', 'Invite', 'discord', 'Discord'])
async def invite(ctx):
    await ctx.send(docs.server_invite)


@client.command()
async def channel(ctx):
    if ctx.author.top_role.permissions.administrator or ctx.author.top_role.permissions.manage_channels:
        chan = str(ctx.channel.id)
        if await q.add_chan(chan):
            main_chans[chan] = False
            await ctx.send(docs.channel_add_successful)
        else:
            main_chans.pop(chan)
            await ctx.send(docs.channel_remove_successful)


### ADMINISTRATION COMMANDS
@client.command()
@is_owner()
async def blist(ctx, *args):
    if len(args) > 0:
        if args[0] in ['ban', 'unban']:
            return await ctx.send(await extras.blist_iface(args, blacklister))
        return await ctx.send(await extras.blist_iface(args[1:], blacklister, password=args[0]))
    return await ctx.send(blacklister.get_blisted())


@client.command()
@is_owner()
async def burn(ctx, *args):  # Not overriding built-in function bin
    if len(args) == 2:
        return await ctx.send(await extras.burn_coins(args))
    await ctx.send(await extras.burn_coins(args[1:], password=args[0]))


@client.command()
@is_owner()
async def wlock(ctx, *args):
    if len(args) == 0:
        return await ctx.send(await extras.toggle_wallet_lock())
    await ctx.send(await extras.toggle_wallet_lock(password=args[0]))


@client.command()
@is_owner()
async def deposits(ctx, *args):
    if len(args) == 0:
        return await ctx.send(extras.toggle_deposits())
    await ctx.send(extras.toggle_deposits(password=args[0]))


@client.command()
@is_owner()
async def stat(ctx, uid):
    await ctx.send(embed=await extras.user_stats(uid))


@client.command()
@is_owner()
async def announce(ctx):
    await extras.do_announce(ctx.message.content.replace('%announce ', ''), docs.announce_title, client)
    await ctx.message.add_reaction(e.WHITE_CHECK)


@client.command()
@is_owner()
async def fban(ctx, uid: str, ban_time: int):
    await q.faucet_ban(uid, ban_time)
    await ctx.message.add_reaction(e.WHITE_CHECK)


@client.command()
@is_owner()
async def reserve(ctx):
    info = await w.query('getinfo', [])
    if isinstance(info, int):
        return
    info['liabilities'] = await q.get_total_creditor_balance()
    info['debt_to_equity'] = round(info['balance'] / info['liabilities'], 3)
    await ctx.send(f'''
Equity: `{info['balance']} GRC`
Stake val: `{info['stake']} GRC`
Liabilities: `{info['liabilities']} GRC`
Ratio: `{info['debt_to_equity']}`
''')

@client.command()
@is_owner()
async def advertise(ctx, email):
    text = ctx.message.content.replace('%advertise', '').replace(f'{email} ', '')
    await q.add_new_advert(email, text)
    await ctx.message.add_reaction(e.WHITE_CHECK)


@client.command()
@is_owner()
async def dadvert(ctx, email):
    await q.delist_advert(email)
    await ctx.message.add_reaction(e.WHITE_CHECK)
###


@client.event
async def on_message(msg):
    cmd = msg.content
    chan = msg.channel
    is_private = isinstance(chan, discord.DMChannel)
    a = msg.author
    uname = a.name
    user = str(a.id)
    iscommand = cmd.startswith(g.pre)

    # Check for if the user is a bot, spamming or is blacklisted
    if a.bot or checkspam(user) or blacklister.is_banned(user, is_private):
        return

    if iscommand and (len(cmd) > 1):
        cmd = cmd[1:]
        log_msg = 'COMMAND "%s" executed by %s (%s)'
        if is_private:
            log_msg = log_msg + ' in private channel'
        logging.info(log_msg, cmd.split()[0], user, uname)
    await client.process_commands(msg)


try:
    with open('API.key', 'r') as key_file:
        API_KEY = str(key_file.read().replace('\n', ''))
    logging.info('API Key loaded')
except Exception:
    logging.error('Failed to load API key')
    exit(1)

client.run(API_KEY)
