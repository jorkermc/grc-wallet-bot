CREATE TABLE IF NOT EXISTS deposits_bak (
    txid VARCHAR(70),
    amount DOUBLE,
    uid VARCHAR(18),
    confirmed BOOLEAN DEFAULT 0,
    PRIMARY KEY (txid, uid)
);


CREATE TABLE IF NOT EXISTS tx_log_bak (
  sender VARCHAR(18),
  receiver VARCHAR(18),
  amount DOUBLE,
  time BIGINT
);


INSERT INTO deposits_bak SELECT * FROM deposits WHERE deposits.confirmed=1;

DELETE FROM deposits WHERE confirmed=1;

INSERT INTO tx_log_bak SELECT * FROM tx_log
WHERE tx_log.time < UNIX_TIMESTAMP()-(24*60*60);

DELETE FROM tx_log WHERE time < UNIX_TIMESTAMP()-(24*60*60);
