import grcconf as g
import emotes as e
import discord


### GENERAL MESSAGES
welcome = f'{e.CELEBRATE}Welcome to the GRC Wallet Bot! Type `%help` for more commands and be sure to read the `%terms` and `%rules`'

faucet_msg = '''The faucet currently contains `{} GRC`.

Donate GRC to this address: `{}` or use `%fgive` to refill the faucet.'''

dm_rain_msg = discord.Embed(title='RAIN!!!', colour=discord.Colour.orange(),
description='You\'ve been rained on! Check your new balance with %bal')

user_data_template = '''```
Address: {}
Balance: {}
Donated: {}
Staked: {}

DM rain messages enabled: {}
Last faucet request (unix): {}
Last transaction (unix): {}
Last TXID out: {}
Last transaction amount: {}```'''

min_deposit = f'**(Min. {g.min_deposit} GRC deposit)**'
balance_template = '{}Your balance for: `{}` ' + min_deposit + '''
```
{} GRC (${} USD)
Unconfirmed: {} GRC
```'''

faucet_thankyou = f'{e.HEART}Thank you for donating to the faucet!'

faq_msg = f'{e.BOOK}The following are currently documented FAQ articles. \n`%faq [selection no.]` '

donation_recipient = '\nYou donated to {}!'

info = discord.Embed(title='This bot is the original work of Delta and various contributors.', colour=discord.Colour.orange(),
description='''
The source code for the bot can be found [here](https://gitlab.com/delta1512/grc-wallet-bot).

If there are any problems, glitches or crashes, please notify me or the contributors below as soon as possible. Any queries can be sent to `boincosdelta@gmail.com`.

To add the bot to your server, use [this link](https://discordapp.com/oauth2/authorize?client_id=406974120580874250&scope=bot&permissions=117824).

Notable mentions:
- [Jorkermc](https://github.com/jorkermc)
- [Chrstphrchvz](https://github.com/chrstphrchvz)
- [Nathanielcwm](https://github.com/nathanielcwm)
- Foxifi
- [LavRadis](https://steemit.com/@lavradis)
''')

claim = '{}You claimed `{} GRC` from the faucet! Your new balance is `{} GRC`\n{}'

server_invite = f'{e.HEART}Come join us over at the Gridcoin Discord! {e.ARR_RIGHT} https://discord.gg/jf9XX4a'

announce_title = 'Announcement from the Wallet Bot Owner'

rain_title = f'{e.RAIN} RAIN!!!! {e.RAIN}'

rain_feedback = f'{e.RAIN} Here comes the rain!'

rain_thankyou = ' The rain pot is now at `{} GRC`'

rain_wait = f'{e.INFO}Please wait for the current rain to process.'

rain_announce_msg = 'Rainbot has rained `{} GRC` on {} users!'

rain_msg = '''The rainbot currently contains `{} GRC` and will rain at `{} GRC`.

Donate GRC to this address `{}`
or type `%rain [amount-GRC]` to build up rain.'''

dm_enabled = f'{e.ON}Direct messages for rain have been enabled.'

dm_disabled = f'{e.OFF}Direct messages for rain have been disabled.'

take_msgs = [
f'{e.HALT1}HALT! You have violated the law. Pay the court a fine or serve your sentence.',
f'{e.HALT2}Nah mate!',
f'{e.GIVE}Sharing is caring, try %give instead.',
f'{e.ROBOT}I\'m sorry, but I\'m afraid I can\'t do that...'
]

stake_template = e.MONEY + 'Your total stake earnings are `{} GRC`'

stake_dm_msg = discord.Embed(title='Stake!', colour=discord.Colour.orange(),
description='The Wallet Bot has staked! Check your earned GRC with `%stake`.')

stake_info_template = '{}It is **estimated** that `{} GRC` will stake in `{} days` and earn `{} GRC` per year.'

withdrawals_offline = f'{e.INFO}Withdrawals are currently offline.'

unimplemented = f'{e.INFO}This command has not been implemented yet, try again later.'

gp_help = f'''{e.INFO}To place a pixel on a grid you must specify the game name, the x and y coordinates and the colour you wish to place.
Example: `%place Delta 1 4 #FF0000` will place a red pixel at (1,4) on the game named "Delta".'''

gp_game_row_template = '[**{}**]({}) ({}x{}) - {} (`{}`)\n\n'

GRC_URL = 'http://www.gridcoin.us/'


### SUCCESS MESSAGES
new_user_success = '{}User account created successfully. Your address is `{}`'

net_tx_success = '{}Transaction of `{} GRC` (inc. {} GRC fee) was successful, ID: `{}`{}'

internal_tx_success = '{}In-server transaction of `{} GRC` was successful.'

channel_add_successful = f'{e.GOOD}This channel has been **enabled** for all commands. To disable this channel, type `%channel` again.'

channel_remove_successful = f'{e.GOOD}This channel has been **disabled** for all commands. To enable, type `%channel` again.'


### ERROR MESSAGES
new_user_fail = f'{e.ERROR}Something went wrong when trying to make your account, please contact the owner.'

already_user = f'{e.CANNOT}Cannot create new account, you already have one.'

fail_dm = f'{e.INFO}It appears the bot cannot PM you.'

cannot_send_self = f'{e.ERROR}You cannot send funds to yourself.'

rule_fail_send = fail_dm + '\nPlease enable direct messages via discord and type `%rules` and `%terms` or check the pinned messages.'

wait_confirm = f'{e.CANNOT}Please wait for your previous transaction to be confirmed.'

invalid_val = f'{e.ERROR}The values provided were invalid.'

invalid_selection = f'{e.ERROR}Invalid selection.'

insufficient_funds = f'{e.ERROR}You have insufficient funds to make that transfer.'

more_than_fee_and_min = f'{e.ERROR}You must provide an amount that is greater than the fee and minimum (`{g.tx_fee + g.MIN_TX} GRC`).'

tx_error = f'{e.ERROR}Error: A transaction could not be made.'

pm_restrict = f'{e.CANNOT}The bot cannot process this command through private messages.'

wallet_data_error = f'{e.ERROR}Could not fetch data.'

change_channel = e.CANNOT + 'Please use the designated channel in this server for that command. (<#{}>)'

no_default_channel = f'{e.INFO}Cannot locate the default channel. Get an admin to type `%channel` in the channel where you want the bot.'

failed_auth = f'{e.ERROR}Authorisation failed.'

address_warning = f'{e.CANNOT}Do not withdraw to walletbot addresses, use `%give` instead.'

failed_fetch = discord.Embed(title='Process failed', colour=discord.Colour.red(),
description=f'{e.ERROR}Failed to fetch data from web server.')

rain_below_min = f'{e.INFO}You must rain at least `{g.MIN_RAIN} GRC`'

rain_needs_more_people = f'{e.INFO}You must rain on at least 3 users.'


### RULES AND TERMS
rules = discord.Embed(title='GRC Wallet Bot Rules', colour=discord.Colour.purple(),
description='''
1. Do not spam the bot in any way.

2. Do not create duplicate or alternate accounts to maximise faucet or rain collections.

3. Do not mislead other users or attempt to steal the funds of other users through technical or social means.

4. Contact the owner of the bot immediately once a bug has been discovered.
''')

terms = discord.Embed(title='GRC Wallet Bot Terms and Conditions', colour=discord.Colour.purple(),
description='''
By holding an account with the bot (ie, after typing %new) you, the user, agree to the following terms of use:

i. You are fully liable for the movement of your funds. Any transaction that occurs to the wrong person or address is not the responsibility of the owner of the bot.

ii. You are fully liable for all funds stored in the bot. **If in the event your funds were lost due to failure, it is not the responsibility of the owner of the bot to reimburse you.**

iii. You agree to act fairly, considering other users, when using the bot and agree to not abuse the 'generous' functionality such as faucet and rain.

iv. You agree to maintain secrecy where possible in the event that you discover a bug with the bot until the owner is contacted and a resolution found.

v. If you fail to follow any rules outlined by the "rules" command, the owner has every right to ban or remove your account.

vi. If you fail to follow Discord Terms of Service or server rules on any Discord server, the owner has every right to ban or remove your account.

vii. If any of the above terms are violated, the owner has every right to ban or remove your account.

***

As the owner of the bot, I agree to the following:

i. To act fairly and in accordance with user opinions in a democratic manner.

ii. In the event of catastrophic failure, explore everything in my capability to recover or reimburse those who have lost their balance.

iii. To have sufficient reason in banning or removing a user account from the bot.
''')

banned = discord.Embed(title='Faucet ban', colour=discord.Colour.red(),
description='''You have been banned from the faucet for not following server rules.
**Please keep faucet messages in the designated channel on your server.**''')
